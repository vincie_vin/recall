package com.vince.recall.viewmodels

import android.os.Build.VERSION_CODES
import com.nhaarman.mockitokotlin2.KArgumentCaptor
import com.nhaarman.mockitokotlin2.argumentCaptor
import com.vince.recall.models.Task
import com.vince.recall.repositories.ITaskRepository
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.any
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [VERSION_CODES.P])
class DashboardViewModelTest {
    @Mock
    private lateinit var repository: ITaskRepository

    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private lateinit var viewModel: DashboardViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        viewModel = DashboardViewModel(repository, testCoroutineDispatcher)
    }

    @Test
    fun testLoadTasksInvokesRepository() = testCoroutineDispatcher.runBlockingTest {
        val taskList = listOf(Task(5, "title", "description", false))
        `when`(repository.getAllTasks()).thenReturn(taskList)
        `when`(repository.getOverallTaskProgress()).thenReturn(0)

        viewModel.loadTasks()

        verify(repository).getAllTasks()

        assertNotNull(viewModel.displayTasks().value)
        assertNotNull(viewModel.taskCompletedProgress().value)
        assertSame(taskList, viewModel.displayTasks().value)
        assertEquals(0, viewModel.taskCompletedProgress().value)
        assertNotNull(viewModel.showProgressBar().value)
        assertFalse(viewModel.showProgressBar().value!!)
    }

    @Test
    fun testCreateTaskInvokesRepository() = testCoroutineDispatcher.runBlockingTest {
        viewModel.createTask("title", "description")

        verify(repository).createTask("title", "description")

        assertNotNull(viewModel.taskCreated().value)
    }

    @Test
    fun testUpdateTask() = testCoroutineDispatcher.runBlockingTest {
        viewModel.updateTask(5,"title", "description", false)
        val taskCaptor: KArgumentCaptor<Task> =  argumentCaptor()

        verify(repository).updateTask(taskCaptor.capture())

        assertEquals(5, taskCaptor.allValues.first().taskId)
        assertEquals("title", taskCaptor.allValues.first().title)
        assertEquals("description", taskCaptor.allValues.first().subtitle)
        assertFalse(taskCaptor.allValues.first().completed)
        assertNotNull(viewModel.taskUpdated().value)
    }

    @Test
    fun testUpdateTaskStatus() = testCoroutineDispatcher.runBlockingTest {
        val taskList = listOf(Task(5, "title", "description", false))
        `when`(repository.getAllTasks()).thenReturn(taskList)
        `when`(repository.getOverallTaskProgress()).thenReturn(0)
        viewModel.updateCompletedStatus(5, false)

        verify(repository).updateTaskStatus(5, false)

        assertNotNull(viewModel.displayTasks().value)
        assertNotNull(viewModel.taskCompletedProgress().value)
        assertSame(taskList, viewModel.displayTasks().value)
        assertEquals(0, viewModel.taskCompletedProgress().value)
    }

    @Test
    fun testDeleteTask() = testCoroutineDispatcher.runBlockingTest {
        `when`(repository.deleteTask(any(Int::class.java))).thenReturn(true)
        viewModel.deleteTask(5)

        verify(repository).deleteTask(5)

        assertNotNull(viewModel.taskDeleted().value)
        assertTrue(viewModel.taskDeleted().value!!)
    }
}