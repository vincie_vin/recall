package com.vince.recall.repositories

import com.vince.recall.TaskEntity
import com.vince.recall.models.Task
import com.vince.recall.storage.TaskDatabase
import com.vince.recall.utils.SingletonHolder

class TaskRepository private constructor(private val taskDatabase: TaskDatabase): ITaskRepository {
    private var taskListCache: List<Task> = emptyList()

    override fun getAllTasks(): List<Task> {
        if (taskListCache.isNotEmpty()) {
            return taskListCache
        }

        val taskDao = taskDatabase.taskDao()
        taskListCache = taskDao.getAllTasks().map { Task(it.id, it.title, it.subtitle, it.completed) }
        return taskListCache
    }

    override fun getOverallTaskProgress(): Int {
        val taskList = getAllTasks()
        if (taskList.isEmpty()) {
            return 0
        }

        val completedTasks = taskList.filter { it.completed }.size
        return if (completedTasks == 0) {
            completedTasks
        } else {
            (taskList.filter { it.completed }.size.toDouble() / taskList.size * 100).toInt()
        }
    }

    override fun createTask(title: String, description: String) {
        val taskDao = taskDatabase.taskDao()
        taskDao.createTask(TaskEntity(0, title, description, false))
        flush()
    }

    override fun updateTask(task: Task) {
        val taskDao = taskDatabase.taskDao()
        taskDao.updateTask(TaskEntity(task.taskId, task.title, task.subtitle, task.completed))
        flush()
    }

    override fun updateTaskStatus(taskId: Int, completed: Boolean) {
        val taskDao = taskDatabase.taskDao()
        taskDao.updateTaskStatus(taskId, completed)
        flush()
    }

    override fun deleteTask(taskId: Int): Boolean {
        val taskList = getAllTasks()
        if (taskList.isEmpty()) {
            return false
        }

        val task = taskList.find { it.taskId == taskId } ?: return false

        val taskDao = taskDatabase.taskDao()
        taskDao.deleteTask(TaskEntity(task.taskId, task.title, task.subtitle, task.completed))
        flush()
        return true
    }

    private fun flush() {
        taskListCache = emptyList()
    }

    companion object: SingletonHolder<TaskRepository, TaskDatabase>(::TaskRepository)
}