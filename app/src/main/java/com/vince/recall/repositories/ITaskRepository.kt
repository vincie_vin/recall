package com.vince.recall.repositories

import com.vince.recall.models.Task

interface ITaskRepository {

    fun getAllTasks(): List<Task>

    fun getOverallTaskProgress(): Int

    fun createTask(title: String, description: String)

    fun updateTask(task: Task)

    fun updateTaskStatus(taskId: Int, completed: Boolean)

    fun deleteTask(taskId: Int): Boolean
}