package com.vince.recall

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.room.Room
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.vince.recall.models.Task
import com.vince.recall.storage.TaskDatabase
import com.vince.recall.utils.Validator
import com.vince.recall.utils.color
import com.vince.recall.viewmodels.DashboardViewModel
import com.vince.recall.viewmodels.DashboardViewModelProviderFactory
import com.vince.recall.views.TaskAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewModel: DashboardViewModel
    private lateinit var validator: Validator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val taskDatabase = Room.databaseBuilder(applicationContext, TaskDatabase::class.java, "tasklist").fallbackToDestructiveMigration().build()
        val viewModelProviderFactory = DashboardViewModelProviderFactory(taskDatabase)
        viewModel = ViewModelProvider(this, viewModelProviderFactory).get(DashboardViewModel::class.java)

        validator = Validator(this)

        val adapter = TaskAdapter(this::updateTaskStatus, this::showUpdateTaskDialog, this::showDeleteTaskDialog)
        task_list.adapter = adapter
        task_list.addItemDecoration(DividerItemDecoration(this, 0))

        add_task_button.setOnClickListener {
            showCreateTaskDialog()
        }

        initObservers(adapter)

        showProgressBar(true)
        viewModel.loadTasks()
    }

    private fun initObservers(adapter: TaskAdapter) {
        viewModel.showProgressBar().observe(this, Observer(this::showProgressBar))
        viewModel.displayTasks().observe(this, Observer { updateView(it, adapter) })
        viewModel.taskCompletedProgress().observe(this, Observer(this::displayTaskProgress))
        viewModel.taskDeleted().observe(this, Observer(this::handleTaskDeleted))
        viewModel.taskCreated().observe(this, Observer {
            showSuccessMessage(getString(R.string.to_do_created))
            viewModel.loadTasks()
        })
        viewModel.taskUpdated().observe(this, Observer {
            showSuccessMessage(getString(R.string.to_do_updated))
            viewModel.loadTasks()
        })
    }

    private fun updateView(taskList: List<Task>, adapter: TaskAdapter) {
        adapter.submitList(taskList)
        if (taskList.isEmpty()) {
            if (view_switcher.currentView != planning_layout) {
                view_switcher.showNext()
            }
        } else {
            if (view_switcher.currentView != task_list) {
                view_switcher.showPrevious()
            }
        }
    }

    private fun displayTaskProgress(progress: Int) {
        task_progressbar.progress = progress
        val progressText = "$progress%"
        progress_stat.text = progressText
    }

    private fun updateTaskStatus(taskId: Int, completed: Boolean) {
        viewModel.updateCompletedStatus(taskId, completed)
    }

    private fun showCreateTaskDialog() {
        showInputDialog(R.string.create_task_dialog_title, R.string.create_task_dialog_subtitle, R.string.button_create) { updatedTitle, updatedDescription -> viewModel.createTask(updatedTitle, updatedDescription)}
    }

    private fun showUpdateTaskDialog(task: Task) {
        showInputDialog(R.string.update_task_dialog_title, R.string.update_task_dialog_subtitle, R.string.button_update, task.title, task.subtitle){ updatedTitle, updatedDescription -> viewModel.updateTask(task.taskId, updatedTitle, updatedDescription, task.completed)}
    }

    private fun showInputDialog(@StringRes dialogTitle: Int, @StringRes dialogSubTitle: Int, @StringRes positiveButtonText: Int, inputTitleText: String = "", inputDescriptionText: String = "", positiveButtonClick: (title: String, description: String) -> Unit) {
        val viewInflated = LayoutInflater.from(this).inflate(R.layout.task_dialog_fragment, window.decorView.rootView as ViewGroup, false)
        val titleInput: TextInputLayout = viewInflated.findViewById(R.id.input_layout_title)
        val descriptionInput: TextInputLayout = viewInflated.findViewById(R.id.input_layout_description)
        if (inputTitleText.isNotBlank()) {
            titleInput.editText?.setText(inputTitleText)
        }
        if (inputDescriptionText.isNotBlank()) {
            descriptionInput.editText?.setText(inputDescriptionText)
        }
        validator.add(titleInput)

        val container = FrameLayout(this)
        val params = FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        params.leftMargin = resources.getDimensionPixelSize(R.dimen.container_side_margin)
        viewInflated.layoutParams = params
        container.addView(viewInflated)

        val inputDialog = AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Dialog_Alert).setTitle(dialogTitle)
            .setMessage(dialogSubTitle)
            .setView(container)
            .setCancelable(false)
            .setPositiveButton(getString(positiveButtonText), null)
            .setNegativeButton(getString(android.R.string.cancel), null)
            .create()
        inputDialog.show()
        inputDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            val title = titleInput.editText?.text.toString().trim()
            val description = descriptionInput.editText?.text.toString().trim()

            if (validator.result()) {
                positiveButtonClick(title, description)
                inputDialog.dismiss()
            }
        }
    }

    private fun showDeleteTaskDialog(task: Task) {
        AlertDialog.Builder(this, android.R.style.Theme_DeviceDefault_Dialog_Alert).setTitle(R.string.delete_task_dialog_title)
            .setMessage(task.title)
            .setPositiveButton(getString(R.string.button_yes)) { _, _ -> viewModel.deleteTask(task.taskId) }
            .setNegativeButton(getString(R.string.button_no), null).show()
    }

    private fun showSuccessMessage(message: String) {
        Snackbar.make(todo_list_activity, message, Snackbar.LENGTH_LONG).color(R.color.colorAccent).show()
    }

    private fun showErrorMessage(message: String) {
        Snackbar.make(todo_list_activity, message, Snackbar.LENGTH_LONG).color(R.color.colorRed).show()
    }

    private fun showProgressBar(show: Boolean) {
        if (show) {
            progressLoader.visibility = View.VISIBLE
        } else {
            progressLoader.visibility = View.GONE
        }
    }

    private fun handleTaskDeleted(taskDeleted: Boolean) {
        if (taskDeleted) {
            showSuccessMessage(getString(R.string.to_do_deleted))
            viewModel.loadTasks()
        } else {
            showErrorMessage(getString(R.string.to_do_delete_error))
        }
    }
}
