package com.vince.recall.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vince.recall.repositories.ITaskRepository
import com.vince.recall.repositories.TaskRepository
import com.vince.recall.storage.TaskDatabase
import kotlinx.coroutines.Dispatchers
import kotlin.coroutines.CoroutineContext

class DashboardViewModelProviderFactory(private val taskDatabase: TaskDatabase): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val taskRepository = TaskRepository.getInstance(taskDatabase)
        return modelClass.getConstructor(ITaskRepository::class.java, CoroutineContext::class.java).newInstance(taskRepository, Dispatchers.IO)
    }
}