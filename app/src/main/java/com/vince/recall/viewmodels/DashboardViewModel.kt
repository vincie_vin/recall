package com.vince.recall.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.vince.recall.models.Task
import com.vince.recall.repositories.ITaskRepository
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class DashboardViewModel(private val repository: ITaskRepository, private val ioContext: CoroutineContext): ViewModel() {

    private val displayTasks = MutableLiveData<List<Task>>()
    fun displayTasks(): LiveData<List<Task>> = displayTasks

    private val taskCreated = MutableLiveData<Any>()
    fun taskCreated(): LiveData<Any> = taskCreated

    private val taskUpdated = MutableLiveData<Any>()
    fun taskUpdated(): LiveData<Any> = taskUpdated

    private val taskDeleted = MutableLiveData<Boolean>()
    fun taskDeleted(): LiveData<Boolean> = taskDeleted

    private val taskCompletedProgress = MutableLiveData<Int>()
    fun taskCompletedProgress(): LiveData<Int> = taskCompletedProgress

    private val showProgressBar = MutableLiveData<Boolean>()
    fun showProgressBar(): LiveData<Boolean> = showProgressBar

    fun loadTasks() {
        viewModelScope.launch(ioContext) {
            taskCompletedProgress.postValue(repository.getOverallTaskProgress())
            displayTasks.postValue(repository.getAllTasks())
            showProgressBar.postValue(false)
        }
    }

    fun createTask(title: String, description: String) {
        showProgressBar.value = true
        viewModelScope.launch(ioContext) {
            repository.createTask(title, description)
            taskCreated.postValue(Any())
        }
    }

    fun updateTask(taskId: Int, title: String, description: String, completed: Boolean) {
        val task = Task(taskId, title, description, completed)
        viewModelScope.launch(ioContext) {
            taskUpdated.postValue(repository.updateTask(task))
        }
    }

    fun updateCompletedStatus(taskId: Int, completed: Boolean) {
        viewModelScope.launch(ioContext) {
            repository.updateTaskStatus(taskId, completed)
            taskCompletedProgress.postValue(repository.getOverallTaskProgress())
            displayTasks.postValue(repository.getAllTasks())
        }
    }

    fun deleteTask(taskId: Int) {
        showProgressBar.value = true
        viewModelScope.launch(ioContext) {
            taskDeleted.postValue(repository.deleteTask(taskId))
        }
    }
}