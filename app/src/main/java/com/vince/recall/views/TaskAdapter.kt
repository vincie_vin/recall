package com.vince.recall.views

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vince.recall.R
import com.vince.recall.models.Task
import kotlinx.android.synthetic.main.todo_list_item.view.*

class TaskAdapter(private val taskCompleteChange: (Int, Boolean) -> Unit,
                  private val taskSelected: (Task) -> Unit,
                  private val taskDelete: (Task) -> Unit): ListAdapter<Task, TaskAdapter.ViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ViewHolder(inflater.inflate(R.layout.todo_list_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val task = getItem(position)
        holder.title.text = task.title
        holder.description.text = task.subtitle
        holder.completed.isChecked = task.completed

        holder.title.setTextColor(if (task.completed) {
            ContextCompat.getColor(holder.itemView.context, R.color.colorAlphaWhite)
        } else {
            Color.WHITE
        })

        holder.description.setTextColor(if (task.completed) {
            ContextCompat.getColor(holder.itemView.context, R.color.colorAlphaWhite)
        } else {
            Color.WHITE
        })

        holder.completed.setOnCheckedChangeListener { _, isChecked ->
            val changePosition = holder.adapterPosition
            val changedTask = getItem(changePosition)
            taskCompleteChange(changedTask.taskId, isChecked)
        }

        holder.delete.setOnClickListener {
            val selectedTask = getItem(holder.adapterPosition)
            taskDelete(selectedTask)
        }

        holder.itemView.setOnClickListener {
            val selectedTask = getItem(holder.adapterPosition)
            taskSelected(selectedTask)
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.task_title
        val description: TextView = itemView.tile_description
        val completed: CheckBox = itemView.task_complete
        val delete: View = itemView.delete_task
    }

    class DiffCallback: DiffUtil.ItemCallback<Task>() {
        override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
            return (oldItem.taskId == newItem.taskId && oldItem.title == newItem.title && oldItem.subtitle == newItem.subtitle && oldItem.completed == newItem.completed)
        }

        override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
            return oldItem == newItem
        }
    }
}