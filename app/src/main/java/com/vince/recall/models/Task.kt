package com.vince.recall.models

data class Task(var taskId: Int = 0, var title: String = "", var subtitle: String = "", var completed: Boolean = false)