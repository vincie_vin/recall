package com.vince.recall.storage

import androidx.room.*
import com.vince.recall.TaskEntity

@Dao
interface TaskDao {

    @Query("SELECT * FROM TaskEntity ORDER BY id DESC")
    fun getAllTasks(): List<TaskEntity>

    @Insert
    fun createTask(task: TaskEntity)

    @Update
    fun updateTask(task: TaskEntity)

    @Query("UPDATE TaskEntity SET completed = :taskCompleted WHERE id = :taskId")
    fun updateTaskStatus(taskId: Int, taskCompleted: Boolean)

    @Delete
    fun deleteTask(task: TaskEntity)


}