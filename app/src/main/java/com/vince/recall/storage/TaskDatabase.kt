package com.vince.recall.storage

import androidx.room.Database
import androidx.room.RoomDatabase
import com.vince.recall.TaskEntity

@Database(entities = [TaskEntity::class], version = 1)
abstract class TaskDatabase: RoomDatabase() {

    abstract fun taskDao(): TaskDao
}