package com.vince.recall.utils

import android.content.Context
import com.google.android.material.textfield.TextInputLayout
import com.vince.recall.R

class Validator(private val context: Context) {
    private var validations: MutableList<TextInputLayout> = mutableListOf()

    fun add(value: TextInputLayout): Validator {
        validations.add(value)
        return this
    }

    fun result(): Boolean {
        validations.map { it.error = null }
        return validateForNotEmpty()
    }

    private fun validateForNotEmpty(): Boolean {
        val check = validations.filter {
            it.editText?.text.isNullOrEmpty()
        }

        check.map {
            it.error = context.resources.getString(R.string.edit_box_empty_error)
        }

        return check.isEmpty()
    }
}